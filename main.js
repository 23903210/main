var pageCounter = 1;
var moduleContainer = document.getElementById('module-info');
var fetchModules = document.getElementById("fetch-modules");

fetchModules.addEventListener("click", function()
{
  var ourRequest = new XMLHttpRequest();
  ourRequest.open('GET', 'https://raw.githubusercontent.com/profharimohanpandey/CW2/master/module-' + pageCounter + '.json');
  ourRequest.onload = function()
  {
    //console.log(ourRequest.responseText);
    var ourData = JSON.parse(ourRequest.responseText);
    //console.log(ourData[0]);
    renderHTML(ourData);
  };

  ourRequest.send();
  pageCounter++;

  if (pageCounter > 3)
  {
    //fetchModules.classList.add("hide-me");
    fetchModules.disabled = true;
  }
});

function renderHTML(data)
{
  var htmlString = "";

  for(i = 0; i < data.length; i++)
  {
    htmlString += "<p>" + data[i].Name + " is a " + data[i].Course + " has assessments "; //".</p>";

    for(j = 0; j < data[i].Module.Assignment.length; j++)
    {
      if (j == 0)
      {
        htmlString += data[i].Module.Assignment[j];
      } else
      {
        htmlString += " and " + data[i].Module.Assignment[j];
      }
    }

    htmlString += ' and Learning Outcome ';

    for(j = 0; j < data[i].Module.Learning_outcomes.length; j++)
    {
      if (j == 0)
      {
        htmlString += data[i].Module.Learning_outcomes[j];
      }
      else
      {
        htmlString += " and " + data[i].Module.Learning_outcomes[j];
      }
    }

    htmlString += ' and Volume ';

    for(j = 0; j < data[i].Module.Volume.length; j++)
    {
      if (j == 0)
      {
        htmlString += data[i].Module.Volume[j];
      }
      else
      {
        htmlString += " and " + data[i].Module.Volume[j];
      }
    }

    htmlString += ' and weights ';

    for(j = 0; j < data[i].Module.weights.length; j++)
    {
      if (j == 0)
      {
        htmlString += data[i].Module.weights[j];
      }
      else
      {
        htmlString += " and " + data[i].Module.weights[j];
      }
    }

    htmlString += '.</p>';
  }
  moduleContainer.insertAdjacentHTML('beforeend', htmlString);
}

// HTML page buttons
var fetchModules = document.getElementById("fetch_modules");
var degreesButton = document.getElementById("degrees");
var modulesButton = document.getElementById("modules");
var assessmentsButton = document.getElementById("assessments");
var createDegreeButton = document.getElementById("create-degree");
var createModuleButton = document.getElementById("create-module");
var createAssessmentButton = document.getElementById("create-assessment");

// HTML page sections
var degreesSection = document.getElementById("degrees-section");
var modulesSection = document.getElementById("modules-section");
var assessmentsSection = document.getElementById("assessments-section");
var createDegreeSection = document.getElementById("create-degree-section");
var createModuleSection = document.getElementById("create-module-section");
var createAssessmentSection = document.getElementById("create-assessment-section");

// JSON data
var degreesData;
var modulesData;
var assessmentsData;

// Request JSON data during page load
requestDegrees();
requestModules();
requestAssessments();

// Degree Programmes JSON data request
function requestDegrees()
{
  var degreesRequest = new XMLHttpRequest();
  degreesRequest.open('GET', 'https://bitbucket.org/23903210/test1/raw/33c2029b2daba381ab404304f6455414dcf8b212/degree.json');
  degreesRequest.onload = function()
  {
    degreesData = JSON.parse(degreesRequest.responseText);
  };
  degreesRequest.send();
}

// Modules JSON data request
function requestModules()
{
  var modulesRequest = new XMLHttpRequest();
  modulesRequest.open('GET', 'https://bitbucket.org/23903210/test1/raw/33c2029b2daba381ab404304f6455414dcf8b212/module.json');

  modulesRequest.onload = function()
  {
    modulesData = JSON.parse(modulesRequest.responseText);
  };
  modulesRequest.send();
}

// Assessments JSON data request
function requestAssessments()
{
  var assessmentsRequest = new XMLHttpRequest();
  assessmentsRequest.open('GET', 'https://bitbucket.org/23903210/test1/raw/33c2029b2daba381ab404304f6455414dcf8b212/assessment.json');
  assessmentsRequest.onload = function()
  {
    assessmentsData = JSON.parse(assessmentsRequest.responseText);
  };
  assessmentsRequest.send();
}


/**
 * Event listener for degrees button
 * Calls function to render the degree programmes page
 */
degreesButton.addEventListener("click", function()
{
  renderDegreesPage(degreesData, modulesData, assessmentsData);
});


/**
 * Event listener for modules button
 * Calls function to render the modules page
 */
modulesButton.addEventListener("click", function()
{
  renderModulesPage(modulesData, assessmentsData);
});


/**
 * Event listener for assessments button
 * Calls function to render the assessments page
 */
assessmentsButton.addEventListener("click", function()
{
  renderAssessmentsPage(assessmentsData);
});


/**
 * Event listener for create degree button
 * Hides the degree programmes page and reveals the create degree page
 */
createDegreeButton.addEventListener("click", function()
{
  degreesSection.hidden = true;
  createDegreeSection.hidden = false;
});


/**
 * Event listener for create module button
 * Hides the modules page and reveals the create module page
 */
createModuleButton.addEventListener("click", function()
{
  modulesSection.hidden = true;
  createModuleSection.hidden = false;
});


/**
 * Event listener for create assessment button
 * Hides the assessments page and reveals the create assessments page
 */
createAssessmentButton.addEventListener("click", function()
{
  assessmentsSection.hidden = true;
  createAssessmentSection.hidden = false;
});


// Renders the degree programmes page
function renderDegreesPage(data, mData, aData)
{
  let html = '';

  for(i = 0; i < data.length; i++)
  {
    html += '\
      <div class="row justify-content-center">\
        <div class="col-md-8">\
          <div class="card mb-3">\
            <div class="card-header text-center" id="heading_'+ i +'">\
              <h5 class="mb-0">\
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapse_'+ i +'" aria-expanded="true" aria-controls="'+ data[i].id +'">\
                '+ data[i].id +': '+ data[i].name +'\
                </button>\
              </h5>\
            </div>\
            <div id="collapse_'+ i +'" class="collapse" aria-labelledby="heading_'+ i +'" data-parent="#degrees-list">\
              <div class="card-body">\
                <div class="row mb-3">\
                  <div class="col-md-6">\
                    <p class="card-text"><b>Learning Outcomes:</b></p>\
                    <ul>\
    ';
    
    html += '\
        </ul>\
      </div>\
    ';

    html += '\
      <div class="col-md-6">\
      <p class="card-text"><b>Exit Awards:</b></p>\
      <ul>\
    ';
    // Loop through degree exit awards
    for(j = 0; j < data[i].exit_awards.length; j++)
    {
      html += '\
        <li>'+ data[i].exit_awards[j] +'</li> \
      ';
    }
    html += '\
          </ul>\
        </div>\
      </div>\
      <div id="modules-list-'+ data[i].id +'">\
    ';

    /**
     * Start of modules
     */
    for(k = 0; k < mData.length; k++)
    {
      if (data[i].id == mData[k].degree_id)
      {
        html += '\
        <div class="row justify-content-center">\
          <div class="col-md-10">\
            <div class="card mb-3">\
              <div class="card-header text-center" id="heading_'+ mData[k].id +'">\
                <h5 class="mb-0">\
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse_'+ mData[k].id +'" aria-expanded="true" aria-controls="'+ mData[k].id +'">\
                  '+ mData[k].id +': '+ mData[k].name +'\
                  </button>\
                </h5>\
              </div>\
              <div id="collapse_'+ mData[k].id +'" class="collapse" aria-labelledby="heading_'+ mData[k].id +'" data-parent="#modules-list-'+ data[i].id +'">\
                <div class="card-body">\
                  <div class="row">\
                    <div class="col-md-6">\
                      <p class="card-text"><b>Learning Outcomes:</b></p>\
                      <ul>\
        ';



        html += '\
                </ul>\
              </div>\
            <div class="col-md-6">\
              <p class="card-text"><b>Number Of Hours:</b> '+ mData[k].number_of_hours +'</p>\
              <p class="card-text"><b>Number Of Credits:</b> '+ mData[k].number_of_credits +'</p>\
            </div>\
          </div>\
          <div class="mt-3 row justify-content-center" id="assessments-list-'+ mData[k].id +'">\
        ';

        for(m = 0; m < aData.length; m++)
        {
          if (aData[m].module_id == mData[k].id)
          {
            html += '\
              <div class="col-md-10">\
                <div class="card mb-3">\
                  <div class="card-header text-center" id="heading_'+ aData[m].id +'">\
                    <h5 class="mb-0">\
                      <button class="btn btn-link" data-toggle="collapse" data-target="#collapse_'+ aData[m].id +'" aria-expanded="true" aria-controls="'+ aData[m].id +'">\
                      '+ aData[m].id +': Assessment '+ aData[m].number +'\
                      </button>\
                    </h5>\
                  </div>\
                  <div id="collapse_'+ aData[m].id +'" class="collapse" aria-labelledby="heading_'+ aData[m].id +'" data-parent="#assessments-list-'+ mData[k].id +'">\
                    <div class="card-body">\
                      <p class="card-text"><b>Learning Outcomes:</b></p>\
                      <ul class="mb-3">\
          ';



          html += '\
            </ul>\
          ';

          html += '\
            <p class="card-text"><b>Volume:</b> '+ aData[m].volume +'</p>\
            <p class="card-text"><b>Weight:</b> '+ aData[m].weight +'</p>\
          ';

          html += '\
                  </div>\
                </div>\
              </div>\
            </div>\
          ';
          }
        }

        html += '\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        ';
      }
    }
      /**
       * End of modules
       */

      html += '\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      ';
    }

    // Insert rendered HTML data to the degree programmes list
    document.getElementById('degrees-list').innerHTML = html;

    // Hide all pages except for degree programmes
    modulesSection.hidden = true;
    assessmentsSection.hidden = true;
    createModuleSection.hidden = true;
    createDegreeSection.hidden = true;
    createAssessmentSection.hidden = true;
    degreesSection.hidden = false;

  }


  // Renders the modules page
  function renderModulesPage(data, aData)
  {
    let html = '';

    for(i = 0; i < data.length; i++)
    {
      html += '\
      <div class="row justify-content-center">\
        <div class="col-md-8">\
          <div class="card mb-3">\
            <div class="card-header text-center" id="m_heading_'+ data[i].id +'">\
              <h5 class="mb-0">\
                <button class="btn btn-link" data-toggle="collapse" data-target="#m_collapse_'+ data[i].id +'" aria-expanded="true" aria-controls="'+ data[i].id +'">\
                '+ data[i].id +': '+ data[i].name +'\
                </button>\
              </h5>\
            </div>\
            <div id="m_collapse_'+ data[i].id +'" class="collapse" aria-labelledby="m_heading_'+ data[i].id +'" data-parent="#modules-list">\
              <div class="card-body">\
                <div class="row">\
                  <div class="col-md-6">\
                    <p class="card-text"><b>Learning Outcomes:</b></p>\
                    <ul>\
      ';



      html += '\
              </ul>\
            </div>\
          <div class="col-md-6">\
            <p class="card-text"><b>Number Of Hours:</b> '+ data[i].number_of_hours +'</p>\
            <p class="card-text"><b>Number Of Credits:</b> '+ data[i].number_of_credits +'</p>\
          </div>\
        </div>\
        <div class="mt-3 row justify-content-center" id="m_assessments-list-'+ data[i].id +'">\
      ';

      for(j = 0; j < aData.length; j++)
      {
        if (aData[j].module_id == data[i].id)
        {
          html += '\
            <div class="col-md-10">\
              <div class="card mb-3">\
                <div class="card-header text-center" id="m_heading_'+ aData[j].id +'">\
                  <h5 class="mb-0">\
                    <button class="btn btn-link" data-toggle="collapse" data-target="#m_collapse_'+ aData[j].id +'" aria-expanded="true" aria-controls="'+ aData[j].id +'">\
                    '+ aData[j].id +': Assessment '+ aData[j].number +'\
                    </button>\
                  </h5>\
                </div>\
                <div id="m_collapse_'+ aData[j].id +'" class="collapse" aria-labelledby="m_heading_'+ aData[j].id +'" data-parent="#m_assessments-list-'+ data[i].id +'">\
                  <div class="card-body">\
                    <p class="card-text"><b>Learning Outcomes:</b></p>\
                    <ul class="mb-3">\
        ';



        html += '\
          </ul>\
        ';

        html += '\
          <p class="card-text"><b>Volume:</b> '+ aData[j].volume +'</p>\
          <p class="card-text"><b>Weight:</b> '+ aData[j].weight +'</p>\
        ';

        html += '\
                </div>\
              </div>\
            </div>\
          </div>\
        ';
      }
    }

    html += '\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
    ';
  }

  // Inserts rendered HTML data to the modules list
  document.getElementById('modules-list').innerHTML = html;

  // Hides all pages except for the modules page
  createModuleSection.hidden = true;
  createDegreeSection.hidden = true;
  createAssessmentSection.hidden = true;
  degreesSection.hidden = true;
  assessmentsSection.hidden = true;
  modulesSection.hidden = false;
}

// Renders the assessments page
function renderAssessmentsPage(data)
{
  let html = '';

  for(i = 0; i < data.length; i++)
  {
    html += '\
      <div class="row justify-content-center">\
        <div class="col-md-8">\
          <div class="card mb-3">\
            <div class="card-header text-center" id="heading_'+ i +'">\
              <h5 class="mb-0">\
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapse_'+ i +'" aria-expanded="true" aria-controls="'+ data[i].id +'">\
                '+ data[i].module_id +': Assessment '+ data[i].number +'\
                </button>\
              </h5>\
            </div>\
            <div id="collapse_'+ i +'" class="collapse" aria-labelledby="heading_'+ i +'" data-parent="#assessments-list">\
              <div class="card-body">\
                <p class="card-text"><b>Assessment ID</b> '+ data[i].id +'</p>\
                <p class="card-text"><b>Learning Outcomes:</b>\
    ';
    // Loop through assessment learning outcomes
    for(j = 0; j < data[i].learning_outcomes.length; j++)
    {
      if (j != data[i].learning_outcomes.length - 1)
      {
        html += '\
          '+ data[i].learning_outcomes[j] +', \
        ';
      }
      else
      {
        html += '\
          '+ data[i].learning_outcomes[j] +' \
        ';
      }
    }
    html += '\
      </p>\
    ';

    html += '\
      <p class="card-text"><b>Volume:</b> '+ data[i].volume +'</p>\
      <p class="card-text"><b>Weight:</b> '+ data[i].weight +'</p>\
    ';

    html += '\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
    ';
  }

  // Inserts rendered HTML into the assessments list
  document.getElementById('assessments-list').innerHTML = html;

  // Hides all pages except for the assessments page
  modulesSection.hidden = true;
  createModuleSection.hidden = true;
  createDegreeSection.hidden = true;
  degreesSection.hidden = true;
  createAssessmentSection.hidden = true;
  assessmentsSection.hidden = false;

}
